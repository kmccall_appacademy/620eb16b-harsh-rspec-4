class Timer

  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    time = @seconds
    string = []
    (0..2).each do |idx|
      string << self.padded(time%60)
      time/=60
      string << ":" unless idx==2
    end
    string.reverse.join
  end

  def padded(num)
      return "0#{num}" if num>=0 && num<10
      "#{num}"
  end


end

class Temperature

  attr_reader :temperature

  def initialize(temperature = {})
    default = {
      f:nil,
      c:nil
    }
    @temperature = default.merge(temperature)
    f = @temperature[:f]
    c = @temperature[:c]
    @temperature[:c]=self.ftoc(f) if c.nil?
    @temperature[:f]=self.ctof(c) if f.nil?
  end

  def self.from_celsius(temp)
    Temperature.new(:c=>temp)
  end

  def self.from_fahrenheit(temp)
    Temperature.new(:f=>temp)
  end

  def ftoc(f_temp)
    c_temp = ((f_temp-32)*5)/9.0
    return c_temp.to_i if c_temp%1==0
    c_temp
  end

  def ctof(c_temp)
    f_temp = (c_temp*9)/5.0 + 32
    return f_temp.to_i if f_temp%1==0
    f_temp
  end

  def in_fahrenheit
    @temperature[:f]
  end

  def in_celsius
    @temperature[:c]
  end

end

class Celsius < Temperature
    def initialize(temp)
      super(:c=>temp)
    end
end

class Fahrenheit < Temperature
    def initialize(temp)
      super(:f=>temp)
    end
end

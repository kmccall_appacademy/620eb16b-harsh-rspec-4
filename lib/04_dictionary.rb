class Dictionary

  attr_reader :entries, :keywords

  def initialize
    @entries = {}
    @keywords = @entries.keys
  end

  def add(option)
    option = {option=>nil} if option.class == String
    @entries = @entries.merge(option).sort_by {|k,v| k}.to_h
    @keywords = @entries.keys
  end

  def include?(key)
    @keywords.include?(key)
  end

  def find(key)
    @entries.select {|entry,value| entry.include?(key)}
  end

  def printable
    final = ""
    @entries.to_a.each_with_index do |set,idx|
      length = @entries.length
      final << "[#{set.first}] \"#{set.last}\""
      final << "\n" unless idx+1==length
    end
    final
  end

end

class Book

  attr_accessor :title

  def title= (title)
    lowercase_words = ["the","a","an", "and", "in", "at", "to", "of"]
    @title =  title.split.map.with_index do |word, idx|
      flag = false
      flag = true unless lowercase_words.include?(word)
      flag = true if idx==0
      word.capitalize! if flag
      word
    end.join(" ")
  end

end
